﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CustomMenuItem
{
#if UNITY_EDITOR
    public class CustomMenuSceneStarter
    {
        [UnityEditor.MenuItem("SceneStart/StartScene &s")]
        // Use this for initialization
        static void StartScene()
        {
            UnityEditor.EditorApplication.OpenScene("Assets/Scenes/0.LogoScene.unity");
            UnityEditor.EditorApplication.isPlaying = true;
        }
    }
#endif
}
