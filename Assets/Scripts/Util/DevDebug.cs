﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Util
{
    /// <summary>
    /// 実機ではADBツールなどでログが表示されないようにカスタマイズ
    /// </summary>
    public class DevDebug
    { 
        static public void Log(object obj)
        {
#if UNITY_EDITOR
            Debug.Log(obj);
#endif
        }

        static public void LogWarning(object obj)
        {
#if UNITY_EDITOR
            Debug.LogWarning(obj);
#endif
        }

        static public void LogError(object obj)
        {
#if UNITY_EDITOR
            Debug.LogError(obj);
#endif
        }

        static public void LogRed(object msg)
        {
#if UNITY_EDITOR
            Debug.Log("<color=red>" + msg + "</color>");
#endif
        }

        static public void LogYellow(object msg)
        {
#if UNITY_EDITOR
            Debug.Log("<color=yellow>" + msg + "</color>");
#endif
        }

        static public void LogGreen(object msg)
        {
#if UNITY_EDITOR
            Debug.Log("<color=green>" + msg + "</color>");
#endif
        }

        static public void LogLogBlue(object msg)
        {
#if UNITY_EDITOR
            Debug.Log("<color=blue>" + msg + "</color>");
#endif
        }

        static public void DrawLine(Vector3 origin, Vector3 direction, Color color, float fDist)
        {
#if UNITY_EDITOR
            Debug.DrawLine(origin, direction, color, fDist);
#endif
        }
    }
}
