﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 相続するChildeSceneではシーンを変更するObjectと、シーン名だけ指定すれば良い
/// </summary>
public class Scene : MonoBehaviour {
    [SerializeField]
    protected FadeCubeCtrl fadeCubeCtrl;
    [SerializeField]
    protected VrModeCtrl vrModeCtrl;

    protected string nextSceneName; //Tableでなんとか出来ないのかな...

    private void Awake()
    {
    }

    private void Start()
    {
        Init();
    }
    public virtual void Init()
    {
        CheckExistTestCam();
        if(vrModeCtrl == null)
        {
            vrModeCtrl = FindObjectOfType<VrModeCtrl>();
        }
        if(fadeCubeCtrl == null)
        {
            fadeCubeCtrl = FindObjectOfType<FadeCubeCtrl>();
        }
        vrModeCtrl.CreateViewModeTextureContainer();
        fadeCubeCtrl.ActiveFadeCube(true);
    }
    private void CheckExistTestCam()
    {
        Util.DevDebug.Log("Check Test Cam Exist");
        GameObject testCamera = GameObject.FindWithTag("TestCamera");
        if(testCamera != null)
        {
            Util.DevDebug.Log("Do Test Cam OFF!");
            testCamera.SetActive(false);
        }
    }
}
