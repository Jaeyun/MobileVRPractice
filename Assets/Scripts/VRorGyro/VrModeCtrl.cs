﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VrModeCtrl : MonoBehaviour
{
    public ViewModeTextureContainer viewModeTexturePrefab;
    [SerializeField]
    private ViewModeTextureContainer viewModeTextureContainer;
    [SerializeField]
    private DeviceOrientation deviceOrientation;
    public GameObject VrModeCamObj;
    public GameObject NormalCamObj;


    public bool isVrModeOn;
    private bool isUIVrModeOn;
    private bool isChangeVrMode;
    private bool isNowProc;
    private bool isUseCountDown;
    private Action ChangeFinishAction;
    private UIRoot uiRoot;

    private void Awake()
    {
    }
    private void Start()
    {
        CreateViewModeTextureContainer();
        ChangeView();
        ChangeViewMode(true, true);
    }

    public void CreateViewModeTextureContainer()
    {
        if(viewModeTextureContainer == null)
        {
            uiRoot = FindObjectOfType<UIRoot>();
            viewModeTextureContainer = Instantiate(viewModeTexturePrefab) as ViewModeTextureContainer;
            viewModeTextureContainer.transform.parent = uiRoot.transform;
            viewModeTextureContainer.transform.localScale = Vector3.one;
        }
        viewModeTextureContainer.SetActiveTexture(false);
    }
    public bool GetIsVrModeON()
    {
        return isVrModeOn;
    }
    public bool GetIsUIVrModeON()
    {
        return isUIVrModeOn;
    }


    public void ChangeViewMode(bool isChangeVr, bool _isUseCountDown, Action changeFinishAction = null)
    {
        isChangeVrMode = isChangeVr;
        isNowProc = true;
        isUseCountDown = _isUseCountDown;
        ChangeFinishAction = changeFinishAction;
        StartCoroutine(ChangeViewModeProc());
    }

    private void ChangeView()
    {
        NormalCamObj.SetActive(!isChangeVrMode);
        VrModeCamObj.SetActive(isChangeVrMode);

        isUIVrModeOn = isChangeVrMode;
        if(isUseCountDown)
        {
            StartCoroutine(CountProc());
            return;
        }
        viewModeTextureContainer.SetActiveTexture(false);
        if(ChangeFinishAction != null)
        {
            ChangeFinishAction();
        }
    }

    IEnumerator ChangeViewModeProc()
    {
        float fCountTime = 0f;
        int maxLength = viewModeTextureContainer.GetChangeGuideImagesLength()- 1;
        int index = isChangeVrMode ? 0 : maxLength;
        if(!viewModeTextureContainer.IsActiveTexture())
        {
            viewModeTextureContainer.SetActiveTexture(true);
        }
        while(isNowProc)
        {
            viewModeTextureContainer.SetchangeGuideImageTexture(index);
            yield return new WaitForFixedUpdate();
            fCountTime += Time.deltaTime;
            if(fCountTime > 0.5f)
            {
                if(isChangeVrMode && index >= maxLength)
                {
                    index = 0;
                }
                else if(!isChangeVrMode && index <= 0)
                {
                    index = maxLength;
                }
                else if(isChangeVrMode && index < maxLength)
                {
                    index++;
                }
                else if(!isChangeVrMode && index > 0)
                {
                    index--;
                }
                fCountTime = 0f;
            }
            Util.DevDebug.LogGreen(isChangeVrMode + " :: " + deviceOrientation);
#if UNITY_EDITOR
            if((isChangeVrMode && deviceOrientation == DeviceOrientation.LandscapeLeft)
                || (!isChangeVrMode && deviceOrientation == DeviceOrientation.Portrait))
            {
                isNowProc = false;
            }
#else
            if((isChangeVrMode && Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
                || (!isChangeVrMode && Input.deviceOrientation == DeviceOrientation.Portrait))
            {
                isNowProc = false;
            }
#endif
        }
        ChangeView();
    }
    IEnumerator CountProc()
    {
        if(!viewModeTextureContainer.IsActiveTexture())
        {
            viewModeTextureContainer.SetActiveTexture(true);
        }
        int nCountTime = 0;
        int maxLength = viewModeTextureContainer.GetCountDownImagesLength() - 1;
        while(nCountTime <= maxLength)
        {
            viewModeTextureContainer.SetCountDownImageTexture(nCountTime);
            nCountTime++;
#if UNITY_EDITOR
            yield return new WaitForSeconds(0.2f);
#else
            yield return new WaitForSeconds(1f);
#endif
        }
        viewModeTextureContainer.SetActiveTexture(false);
        isVrModeOn = isChangeVrMode;
        if(ChangeFinishAction != null)
        {
            ChangeFinishAction();
        }
        Util.DevDebug.LogGreen("isVrModeOn :: " + isVrModeOn + ", isChangeVrMode :: " + isChangeVrMode);
    }
}
