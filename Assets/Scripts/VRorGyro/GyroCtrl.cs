﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroCtrl : MonoBehaviour
{
    private Gyroscope gyro;
    private bool gyroCheck;
    private Quaternion rot;
    public Transform trContainer;
    public Transform trPlayer;
    public DeviceOrientation deviceOrientation;

    private void Awake()
    {

    }
    void Start()
    {
        //transform.position = trContainer.position;
        //transform.forward = trContainer.forward;
        //transform.SetParent(trPlayer);
        gyroCheck = GyroCheck();
    }

    private bool GyroCheck()
    {
        if(SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
            transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
            transform.LookAt(trPlayer);
            rot = new Quaternion(0f, 0f, 1f, 0);
            deviceOrientation = Input.deviceOrientation;
            return true;
        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!gyroCheck)
        {
            Util.DevDebug.LogWarning("Gyro Not Supported!");
            return;
        }
        transform.localRotation = Quaternion.Euler(90f, 0f, 0f) * gyro.attitude * rot;
    }
}
