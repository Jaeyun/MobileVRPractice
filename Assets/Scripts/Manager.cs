﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {
    [SerializeField]
    private PlayerCtrl playerCtrl;
	// Use this for initialization
	private void Awake () {
        DontDestroyOnLoad(playerCtrl);
        DontDestroyOnLoad(this);
    }
}
