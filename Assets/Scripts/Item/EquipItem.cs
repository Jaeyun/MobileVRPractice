﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EquipType
{
    None = 0,
    Armor = 1,
    Weapon,
    Legs,
    Glove,
    Helm
}
/// <summary>
/// 装備アイテムのクラス。
/// 2018.01.08 @Choi
/// </summary>
public class EquipItem : MonoBehaviour
{
    private string id;
    private string equipName;
    private string desc;
    private EquipType equipType;
    private int nOffenceVal;
    private int nDefenceVal;
    private int nDexVal;
    private bool isEquipedNow;
    private bool isHaveNow;

    #region Parameter Setting
    public string GetID()
    {
        return id;
    }
    public void SetID(string val)
    {
        id = val;
    }

    public string GetName()
    {
        return equipName;
    }
    public void SetName(string val)
    {
        equipName = val;
    }

    public string GetDesc()
    {
        return desc;
    }
    public void SetDesc(string val)
    {
        desc = val;
    }

    public EquipType GetEquipType()
    {
        return equipType;
    }
    public void SetEquipType(int val)
    {
        switch(val)
        {
            case (int)EquipType.Armor:
                equipType = EquipType.Armor;
                break;

            case (int)EquipType.Glove:
                equipType = EquipType.Glove;
                break;

            case (int)EquipType.Helm:
                equipType = EquipType.Helm;
                break;

            case (int)EquipType.Legs:
                equipType = EquipType.Legs;
                break;

            case (int)EquipType.Weapon:
                equipType = EquipType.Weapon;
                break;

            default:
                equipType = EquipType.None;
                break;
        }
    }

    public int GetOffenceVal()
    {
        return nOffenceVal;
    }
    public void SetOffenceVal(int val)
    {
        nOffenceVal = val;
    }

    public int GetDefenceVal()
    {
        return nDefenceVal;
    }
    public void SetDefenceVal(int val)
    {
        nDefenceVal = val;
    }

    public int GetDexVal()
    {
        return nDexVal;
    }
    public void SetDexVal(int val)
    {
        nDexVal = val;
    }

    public bool GetSetIsEquipedNow
    {
        get { return isEquipedNow; }
        set { isEquipedNow = value; }

    }
    #endregion
}
