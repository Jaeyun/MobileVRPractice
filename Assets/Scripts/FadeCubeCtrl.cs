﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeCubeCtrl : MonoBehaviour
{
    [SerializeField]
    private GameObject objFadecube;
    private Renderer fadeCubeRenderer;
    private float fFadeTime = 2f;

    private Action fadeOutAction;
    private Action fadeInAction;

    private void Start()
    {
        fadeCubeRenderer = objFadecube.GetComponent<Renderer>();
    }

    public void DoFadeOutInAction(Action _fadeOutAction, Action _fadeInAction)
    {
        fadeOutAction = _fadeOutAction;
        fadeInAction = _fadeInAction;
        StartCoroutine(FadeOutInProc());
    }

    public void DoFadeOutAction(Action _fadeOutAction)
    {
        fadeOutAction = _fadeOutAction;
        StartCoroutine(FadeOutProc());
    }

    public void DoFadeInAction(Action _fadeInAction)
    {
        fadeInAction = _fadeInAction;
        StartCoroutine(FadeInProc());
    }

    public void ActiveFadeCube(bool isActive)
    {
        objFadecube.SetActive(isActive);
    }

    IEnumerator FadeOutInProc()
    {
        float fTime = 0f;
        if(!objFadecube.activeSelf)
        {
            objFadecube.SetActive(true);
        }
        while(fTime < 1f)
        {
            fTime += Time.deltaTime / fFadeTime;
            fadeCubeRenderer.material.color = new Color(0f, 0f, 0f, fTime);
            yield return new WaitForEndOfFrame();
        }
        fadeCubeRenderer.material.color = new Color(0f, 0f, 0f, 1f);
        fTime = 0f;
        if(fadeOutAction != null)
        {
            fadeOutAction();
        }


        while(fTime < 1f)
        {
            fTime += Time.deltaTime / fFadeTime;
            fadeCubeRenderer.material.color = new Color(0f, 0f, 0f, 1f - fTime);
            yield return new WaitForEndOfFrame();
        }
        fadeCubeRenderer.material.color = new Color(0f, 0f, 0f, 0f);
        objFadecube.SetActive(false);
        if(fadeInAction != null)
        {
            fadeInAction();
        }
    }

    IEnumerator FadeOutProc()
    {
        float fTime = 0f;
        if(!objFadecube.activeSelf)
        {
            objFadecube.SetActive(true);
        }
        while(fTime < 1f)
        {
            fTime += Time.deltaTime / fFadeTime;
            fadeCubeRenderer.material.color = new Color(0f, 0f, 0f, fTime);
            yield return new WaitForEndOfFrame();
        }
        fadeCubeRenderer.material.color = new Color(0f, 0f, 0f, 1f);
        fTime = 0f;
        if(fadeOutAction != null)
        {
            fadeOutAction();
        }
    }

    IEnumerator FadeInProc()
    {
        float fTime = 0f;
        if(fadeInAction != null)
        {
            fadeInAction();
        }
        if(!objFadecube.activeSelf)
        {
            objFadecube.SetActive(true);
        }
        fadeCubeRenderer.material.color = new Color(0f, 0f, 0f, 1f);
        while(fTime < 1f)
        {
            fTime += Time.deltaTime / fFadeTime;
            fadeCubeRenderer.material.color = new Color(0f, 0f, 0f, 1f - fTime);
            yield return new WaitForEndOfFrame();
        }
        fadeCubeRenderer.material.color = new Color(0f, 0f, 0f, 0f);
        objFadecube.SetActive(false);
    }
}
