﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UILookSceneChangeButton : UILookInteraction
{
    private string targetSceneName;
    [SerializeField]
    private FadeCubeCtrl fadeCubeCtrl;
    
    public void SetData(string SceneName)
    {
        targetSceneName = SceneName;
        Init();
        GetSetIsOnceEvent = true;
    }

    public override void UIAction()
    {
        base.UIAction();
        fadeCubeCtrl.DoFadeOutInAction(NextSceneALoadction, null);
    }

    private void NextSceneALoadction()
    {
        SceneManager.LoadScene(targetSceneName);
    }
}
