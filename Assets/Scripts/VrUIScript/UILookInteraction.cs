﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// LookAtUICheckスクリプトとペアになるのが望ましい。(なくてもInit()で追加してくれるが、AddComponent Callを防ぎたい。)
/// Child関係のスクリプトではisOnceEvent変数を手動で指定すること。
/// Childe関係のObjectでは必ずｆLookMaxTimeを指定すること。
/// 2018.01.03 @Choi
/// </summary>
public class UILookInteraction : MonoBehaviour
{
    private LookAtUICheck lookAtUICheck;
    private float fLookTime;
    public float fLookMaxTime;
    private bool isOnceEvent;

    protected void Start()
    {
        Init();
    }

    protected void Update()
    {
        if(lookAtUICheck == null)
        {
            return;
        }

        if(!lookAtUICheck.GetSet_isCamLookAtMe)
        {
            UINoneLookingAction();
            return;
        }
        UILookingAction();
    }

    public virtual void Init()
    {
        fLookTime = 0f;
        lookAtUICheck = GetComponent<LookAtUICheck>();
        if(lookAtUICheck == null)
        {
            gameObject.AddComponent<LookAtUICheck>();
            lookAtUICheck = GetComponent<LookAtUICheck>();
        }
    }

    public virtual void UILookingAction()
    {
        fLookTime += Time.deltaTime;
        if(fLookTime > fLookMaxTime)
        {
            UIAction();
            lookAtUICheck.GetSet_isCamLookAtMe = false;
            fLookTime = 0f;
        }
    }

    public virtual void UINoneLookingAction()
    {
        fLookTime = 0f;
    }

    public virtual void UIAction()
    {
        if(isOnceEvent)
        {
            GetComponent<Collider>().enabled = false;
        }
    }

    public bool GetSetIsOnceEvent
    {
        get { return isOnceEvent; }
        set { isOnceEvent = value; }
    }
}
