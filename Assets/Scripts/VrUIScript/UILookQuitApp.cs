﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILookQuitApp : UILookInteraction {
    VrModeCtrl vrModeCtrl;
    FadeCubeCtrl fadeCubeCtrl;
    public override void Init()
    {
        base.Init();
        fLookMaxTime = 2f;
        GetSetIsOnceEvent = true;
        vrModeCtrl = FindObjectOfType<VrModeCtrl>();
        fadeCubeCtrl = FindObjectOfType<FadeCubeCtrl>();
    }

    public override void UIAction()
    {
        base.UIAction();
        fadeCubeCtrl.DoFadeInAction(ChangeViewAction);
    }

    private void ChangeViewAction()
    {
        vrModeCtrl.ChangeViewMode(false, true, ()=> {
            Util.DevDebug.LogRed("어플 종료");
            Application.Quit();
        });
    }
}
